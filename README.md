# ⚡️ vite electron esbuild starter

Installation dependencies

```shell
yarn install
```
Start local development
```shell
# Use esbuild to compile the main process Typescript, which is faster
yarn run dev
```

Compile/Pack

```shell
# Only build the target code and resources of the main process and the rendering process, without packaging (exe, dmg, etc.)
yarn run build

# Preview your application in production mode without pack.
yarn run preview

# Build and pack as a windows .exe installer
yarn run pack:win
```

Clean up the build directory

```shell
yarn run clean
```



## File structure

Use [two-package-structure](https://www.electron.build/tutorials/two-package-structure)

```
+ app                     electron-builder app directory and its build product directory (target js code, image resources, etc., instead of installation packages or executable files)
  - package.json          Production dependencies, all stored as dependencies (not devDependencies)
+ dist                    electron-builder package directory
+ scripts                 Support scripts for development/build.
+ src      
  + common                common code
  + main                  for main process
  + renderer              for renderer process
- package.json            Dependencies during development, all stored as devDependencies (not dependencies)
- vite.config.ts          vite configurations
- electron-builder.json    electron-builder configurations
```
