// Описание файла: инициализация окна приложения, а также работа с консолью.

"use strict";

import { app, BrowserWindow, Menu, screen } from "electron";
import path from "path";
import child_process from "child_process";

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */

import util from "util";

let isDevelopment = process.env.NODE_ENV == "development";

let mainWindow;
let printWindow = null;
/*let childWindow
const childWinURL = process.env.NODE_ENV === 'development'
  ? 'http://localhost:9080/#/categoryResults'
  : `file://${__dirname}/index.html#categoryResults`*/

const gotTheLock = app.requestSingleInstanceLock();

if (!gotTheLock) {
  app.quit();
} else {
  app.on("second-instance", (event, commandLine, workingDirectory) => {
    // Someone tried to run a second instance, we should focus our window.
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore();
      mainWindow.focus();
    }
  });

  app.on("ready", async () => {
    await createMainWindow();
    await createPrintWindow();
  });

  app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
      app.quit();
    }
  });
}

async function createMainWindow() {
  const display = screen.getPrimaryDisplay();
  let area = display.workArea;

  mainWindow = new BrowserWindow({
    useContentSize: false,
    kiosk: !isDevelopment,
    width: area.width,
    height: area.height,
    autoHideMenuBar: true,
    show: true,
    webPreferences: {
      webSecurity: false, // теперь можно ссылаться на локальные файлы file://
      nodeIntegration: true, // to access 'require' feautures in "browser" code
      // nodeIntegrationInWorker: true,
      enableRemoteModule: true, // to access some other 'require' feautures in "browser" code
      contextIsolation: false, // Для более удобной отладки
      // nodeIntegrationInWorker: true
      // nodeIntegrationInSubFrames: true
    },
  });

  mainWindow.maximize();
  mainWindow.closeDevTools();

  mainWindow.once('ready-to-show', () => {
    mainWindow.webContents.setZoomFactor(area.width/1920)
    // mainWindow.show()
})

  if (isDevelopment) {
    await mainWindow.loadURL("http://localhost:3000");
    // mainWindow.webContents.toggleDevTools();
  } else {
    await mainWindow.loadURL(`file://${__dirname}/renderer/index.html`);
  }

  mainWindow.on("close", () => {
    mainWindow = null;
    printWindow?.close();
  });

  mainWindow.on("closed", () => {
  });

}

async function createPrintWindow() {
  const display = screen.getPrimaryDisplay();
  let area = display.workArea;

  printWindow = new BrowserWindow({
    useContentSize: true,
    show: false,
  });

  printWindow.on("close", () => {
    printWindow = null;
  });

}

// Menu.setApplicationMenu(null)

export const execFile = util.promisify(child_process.execFile);
export const exec = util.promisify(child_process.exec);
