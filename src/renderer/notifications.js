import { toast } from 'react-toastify';

let lastSuccess, lastInfo, lastSystemError, lastUserError;

export function showSuccess(text, options = {}) {
  lastSuccess = toast.success(text, { autoClose: 3000, ...options });
}
window.showSuccess = showSuccess;

export function showInfo(text, options = {}) {
  lastInfo = toast.info(text, { autoClose: 4000, ...options });
}
window.showInfo = showInfo;

export function showSystemError(text, options = {}) {
  lastSystemError = toast.error(text, { autoClose: 10000, ...options });
}
window.showSystemError = showSystemError;

export function showUserError(text, options = {}) {
  lastUserError = toast.warning(text, { autoClose: 5000, ...options });
}
window.showUserError = showUserError;
