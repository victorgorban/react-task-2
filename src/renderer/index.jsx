import React from "react"

import ReactDOM from "react-dom";
import App from "@components/App";
import dayjs from "dayjs";
import "dayjs/locale/ru";
import weekday from "dayjs/plugin/weekday";
import customParseFormat from "dayjs/plugin/customParseFormat";

import handleIpc from '@renderer/handleIpc'



dayjs.extend(customParseFormat);
dayjs.locale("ru");
dayjs.extend(weekday);

/* import remote from '@electron/remote' // не работает import и require remote и все тут. Придется использовать ipcRenderer

remote.require('fs') */

handleIpc()

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
