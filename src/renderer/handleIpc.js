import { setGlobalInfo, $nfcStatus } from "./stores/global";

const { ipcRenderer } = require("electron");

import * as notifications from "@renderer/notifications";

let isIpcInited = false;

export default function handleIpc() {
  console.log("in renderer handleIpc");

  ipcRenderer.on("startup", (event, params) => {
    if (!isIpcInited) {
      isIpcInited = true;
      console.log("startup init called", params);

      initIpc();
    } else {
      isIpcInited = true;
    }
  });

  function initIpc() {
    ipcRenderer.send("getAppInfo_from_renderer", "test2");

    ipcRenderer.on("getAppInfo_to_renderer", (event, params) => {
      console.log("got getAppInfo_to_renderer. Set global info", params);
      let appInfo = JSON.parse(params);
      let oldGlobalInfo = window.$globalInfo || {};
      setGlobalInfo({ ...oldGlobalInfo, ...appInfo });
    });

    ipcRenderer.on("autoUpdate_to_renderer", (event, message) => {
      console.log("got autoUpdate_to_renderer", message);
      if (!message.startsWith("Загружается обновление")) {
        notifications.showInfo(message, { position: "bottom-left" });
      }
    });

    ipcRenderer.on("nfc_to_renderer", (event, message) => {
      console.log("got nfc_to_renderer", message);
      let messageObject = JSON.parse(message);
      console.log("messageObject", messageObject);
      let messageData = messageObject.data;
      let messageMessage = messageData.message;
      let messageDataData = messageData.data;
      switch (messageObject.type) {
        case "success": {
          console.info('nfc success', messageMessage, messageDataData)
          break;
        }
        case "status": {
          $nfcStatus.api.replace(messageData)
          break;
        }
        case "info": {
          notifications.showInfo(messageMessage)
          console.info('nfc info', messageMessage, messageDataData)
          break;
        }
        case "error": {
          notifications.showSystemError(messageMessage)
          console.error('nfc error', messageMessage, messageDataData)
          break;
        }
      }
    });
  }
}
