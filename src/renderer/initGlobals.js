import { setRawCategories, setTara, setProcess, setFishTypes } from "@renderer/stores/global";

import * as helpers from "@renderer/helpers";
import * as notifications from "@renderer/notifications";

export default async function initGlobals() {
  async function getCats() {
    let result = await helpers.submitObject(
      "getCats",
      {}
    );
    if (result.status == "error") {
      let localStored = localStorage.getItem("$rawCategories") || "[]";

      setTara(JSON.parse(localStored));
      throw new Error(result.message);
    }
    setRawCategories(result.data);
    localStorage.setItem("$rawCategories", JSON.stringify(result.data));
  }

  async function getTara() {
    let result = await helpers.submitObject(
      "getTara",
      {}
    );
    if (result.status == "error") {
      let localStored = localStorage.getItem("$tara") || "[]";

      setTara(JSON.parse(localStored));
      throw new Error(result.message);
    }
    setTara(result.data);
    localStorage.setItem("$tara", JSON.stringify(result.data));
  }

  async function getProcess() {
    let result = await helpers.submitObject(
      "getProcess",
      {}
    );
    if (result.status == "error") {
      let localStored = localStorage.getItem("$process") || "[]";

      setProcess(JSON.parse(localStored));
      throw new Error(result.message);
    }
    setProcess(result.data);
    localStorage.setItem("$process", JSON.stringify(result.data));
  }

  async function getFishTypes() {
    let result = await helpers.submitObject(
      "getFishTypes",
      {}
    );
    if (result.status == "error") {
      let localStored = localStorage.getItem("$fishTypes") || "[]";

      setFishTypes(JSON.parse(localStored));
      throw new Error(result.message);
    }
    setFishTypes(result.data);
    localStorage.setItem("$process", JSON.stringify(result.data));
  }

  try {
    await Promise.all([getCats(), getTara(), getProcess(), getFishTypes()]);
  } catch (e) {
    console.error(e);
    notifications.showSystemError(
      "Произошла ошибка при получении глобальных данных. Используются раннее сохраненные данные. Откройте консоль для получения дополнительной информации",
      {
        autoClose: 1000 * 60 * 60 * 24 * 30, //месяц будет висеть, пока не закроют явно
      }
    );
    return;
  }
}
