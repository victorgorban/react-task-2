import React, { useEffect, useState, useRef } from "react";
import dayjs from "dayjs";
import convertRu from "convert-layout/ru";

export { default as submitObject } from "./submitObject";

export { dayjs };

export function redirectLink(link, history) {
  history.push(link);
}

export function goBack(history) {
  console.log("goBack, history", history);
  window.historyReact = history;
  if (history.length > 1) {
    history.goBack();
  } else {
    history.push("/");
  }
}

export function randomInt(min = 0, max = 1_000_000_000) {
  // случайное число от min до (max+1)
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

export function randomIntString(min = 0, max = 9, length = 10) {
  // случайное число от min до (max+1)
  let result = "";
  for (let i = 0; i < length; i++) {
    result += randomInt(min, max);
  }
  return result;
}

// % number1 of number2
export function getPercent(number1, number2, defaultValue, formatter) {
  let percent = (number1 / number2) * 100;
  console.log("percent", percent, number1, number2);
  if (!percent) {
    percent = defaultValue;
  } else {
    percent = formatter(percent);
  }
  return percent;
}

export function dateDiff(date1, date2, unit) {
  let result = dayjs(date1).diff(date2 || new Date(), unit || "years");
  console.log("dateDiff result", result);
  return result;
}

export function formatDate(date, format) {
  if (!date) return "";
  /* date = new Date(date) */
  let result = dayjs(date).format(format || "DD.MM.YYYY");
  if (result == "Invalid Date") {
    // console.log('format date, returned', date)
    return date;
  }
  // console.log('formatDate', date, result);
  return result;
}

export function formatDateFull(date, format) {
  if (!date) return "";
  let result = dayjs(date).format(format || "HH:mm:ss DD.MM.YYYY");
  if (result == "Invalid Date") {
    return date;
  }
  return result;
}

export function parseDate(string, format) {
  // let dayjsFormat = dayjs(string, "DD.MM.YYYY").format("DD.MM.YYYY");
  // console.log('parseDate', string, dayjsFormat);
  let result = dayjs(string, format || "DD.MM.YYYY").toDate();
  if (result == "Invalid Date") {
    // console.log('parse date, returned', string)
    return string;
  }
  // console.log('parseDate result', result);
  return result; // почему-то парсится в дату чуть раньше
}

export function parseTime(string, format) {
  let result = dayjs(string, format || "DD.MM.YYYY HH:mm").toDate();
  // console.log('parseTime', string, format, result);

  return result; // почему-то парсится в дату чуть раньше
}

export function formatTime(date, format) {
  if (!date) return "";
  let result = dayjs(date).format(format || "HH:mm:ss");
  // console.log("formatTime", date, format, result);

  return result;
}

export function formatBool(bool) {
  return bool ? "Да" : "Нет";
}

export function preventTab(e) {
  // console.log("preventTab", e);
  if (e.keyCode == 9 && !e.shiftKey) {
    e?.preventDefault();
    e?.stopPropagation();
    return false;
  }
  return e.keyCode;
}

export function preventShiftTab(e) {
  // console.log("preventTab", e);
  if (e.keyCode == 9 && e.shiftKey) {
    e?.preventDefault();
    e?.stopPropagation();
    return false;
  }
  return e.keyCode;
}

export function readFileText(file) {
  console.log("in readFileText");
  return new Promise((resolve, reject) => {
    let reader = new FileReader();

    reader.readAsText(file);

    reader.onload = function () {
      resolve(reader.result);
    };

    reader.onerror = function () {
      reject(reader.error);
    };
  });
}

export function handleEnterScan(e, handleScanHandler, isFullUrl = false) {
  let el = e.currentTarget;
  let value = el.value.trim();

  let convertedValue = ruToEn(value);
  if (!isFullUrl) {
    try {
      let url = new URL(convertedValue);
      let toSplit = url.search || url.pathname;
      let split = _.split(toSplit, "/");
      toSplit = split[split.length - 1];
      if (!toSplit) throw new Error("nothing to split");
      split = _.split(toSplit, "=");
      // console.log('split2', split)
      convertedValue = split[split.length - 1];
      // console.log(convertedValue)
    } catch (e) {
      console.log(""); // это не url, ну и ок
    }
  }

  el.value = "";

  handleScanHandler(convertedValue);
}

export function ruToEn(value) {
  let converted = value;
  const cyrillicPattern = /[а-яА-ЯЁё]/;
  let isRussian = cyrillicPattern.test(value);
  if (isRussian) {
    converted = convertRu.toEn(value);
  }
  console.log(
    "ruToEn, isRussian, value, converted",
    isRussian,
    value,
    converted
  );

  return converted;
}
window.ruToEn = ruToEn;

export const taraTypesMap = {
  metal: "металл",
  glass: "стекло",
};

export function weightFromGramsByNetto(tara, netto) {
  switch (netto) {
    case "g":
      return tara;
    case "oz":
      return tara / 28;
    default:
      return tara;
  }
}
