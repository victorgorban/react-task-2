import { $requestsPending } from "@renderer/stores/global";

export default async function submitObject(
  url,
  data = {},
  { timeout = 3000, maxAttemps = 3, baseUrl = 'https://diana.crp.st/api' } = {}
) {
  try {
    let response = null;

    console.log("submitObject", url, data);
    $requestsPending.api.increase(1);

    for (let i = 0; i < maxAttemps; i++) {
      try {
        const controller = new AbortController();
        //* timeout:
        const timeoutId = setTimeout(() => controller.abort(), timeout);
        response = await fetch(baseUrl + "/" + url, {
          signal: controller.signal,
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: JSON.stringify(data), // body data type must match "Content-Type" header
        });
        clearTimeout(timeoutId);
      } catch (e) {
        // если ошибка таймаута, то пробуем еще пару раз. Если другая ошибка, то сразу отмена
        if (i < maxAttemps && e.message == "The user aborted a request.") {
          continue;
        } else {
          throw e;
        }
      }

      //* если успех, то прерываем цикл попыток
      break;
    }

    if (response.status != 200) {
      console.log("response error", response);
      throw new Error(response.statusText || "Http sending error");
    }

    let postResult = await response.json();

    if (postResult.status == "error") {
      throw new Error(postResult.message || JSON.stringify(postResult));
    }

    console.log("submitObject result", postResult);

    return postResult;
  } catch (e) {
    throw e;
  } finally {
    $requestsPending.api.decrease(1);
  }
}
window.submitObject = submitObject;
