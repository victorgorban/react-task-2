import React, { useEffect, useState, useRef } from "react";
import {
  HashRouter,
  BrowserRouter,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { useStore } from "effector-react";
import { $loginData } from "@renderer/stores/global";
import { globalReset } from "@renderer/stores/global";

import { ToastContainer, Flip as FlipTransition, Zoom as ZoomTransition, Slide as SlideTransition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import "@assets/css/index.css";

import '@renderer/notifications'


import MainMenu from "@components/_root/MainMenu";
import Settings from "@components/_root/Settings";
import Page404 from "@components/_root/Page404";
import Login from "@components/_root/Login";
import Help from "@components/_root/Help";
import PageNotAllowed from "@components/_root/PageNotAllowed";


import ModeTech_Main from "@renderer/components/ModeTech/Main";

import initGlobals from '@renderer/initGlobals'

export default function App() {
  /*   useEffect(()=>{
  
    }, [])
   */

  useEffect(() => {
    initGlobals();
    return function clear() {
      globalReset()
    }
  }, [])

  function renderWithRights(router, component) {
    let isAllowed = false;

    function setAllowed() { // проверка прав доступа
      let publicPages = [
        '/',
        '/login',
        '/help',
        '/404',
        '/not_allowed',

      ]

      let path = router.match.path;
      let params = router.location.state;
      // console.error('setAllowed, path, params, publicPages', path, publicPages, publicPages.includes(path))
      if (publicPages.includes(path)) {
        return isAllowed = true;
      }

      return isAllowed = true; // пока что без ролей



    }
    setAllowed();

    // console.log('isAllowed', isAllowed)
    if (isAllowed)
      return component;
    else return (<PageNotAllowed />);
  }


  return (
    <div className="d-flex flex-column main-wrapper">
      <div className={'w-100'}>
        {
          <HashRouter>
            <Switch>
              <Route path="/" render={(router) => renderWithRights(router, <Login {...router.location.state} />)} exact />
              <Route path="/menu" render={(router) => renderWithRights(router, <MainMenu {...router.location.state} />)} exact />
              <Route path="/settings" render={(router) => renderWithRights(router, <Settings {...router.location.state} />)} exact />

              {/* технолог */}
              <Route path="/mode_tech/main" render={(router) => renderWithRights(router, <ModeTech_Main {...router.location.state} />)} exact />
              {/* endof технолог */}

              <Route path="/help" render={(router) => renderWithRights(router, <Help {...router.location.state} />)} exact />
              <Route path="/404" render={(router) => renderWithRights(router, <Page404 {...router.location.state} />)} exact />
              <Route path="*" component={Page404} exact />

            </Switch>
          </HashRouter>
        }
      </div>
      <ToastContainer
        position="bottom-right"
        autoClose={5000}
        theme="colored"
        transition={SlideTransition}
        newestOnTop={true}
        closeOnClick={true}
        closeButton={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}
