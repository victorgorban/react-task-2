import React, { useEffect, useState, useRef } from "react";
import { useHistory } from "react-router-dom";
import { useStore } from "effector-react";
import { $userData } from "@renderer/stores/global";
import { $globalInfo } from "@renderer/stores/global";

import TopBar from '@components/_root/TopBar'

// не работает import electron-remote адекватно и все тут. require тоже, window.require тоже. Ладно, буду использовать через ipcRenderer

export default function Help() {
  //* библиотеки и неизменяемые значения
  const history = useHistory();
  let title = "Помощь"
  //* endof библиотеки и неизменяемые значения


  //* глобальное состояние из store
  let userData = useStore($userData);
  let globalInfo = useStore($globalInfo);
  //* endof глобальное состояние из store

  //* эффекты
  useEffect(() => {
    document.title = title;
  }, []);
  //* endof эффекты

  //* вспомогательные функции, НЕ ОБРАБОТЧИКИ
  function instructionByUser() {
    switch (userData.role_id) {
      case 2:
      case 3:
        return "http://31.133.58.73:30194/files/instruction_spo.pdf";
      case 4:
      case 5:
      case 6:
        return "http://31.133.58.73:30194/files/instruction_vpo.pdf";
      case 8:
        return "http://31.133.58.73:30194/files/instruction_applicant.pdf";
      default:
        return "http://31.133.58.73:30194/files/instruction.pdf";
    }
  }

  function getCurrentAppVersion() {
    let currentVersion = "";
    if (process.env.NODE_ENV !== "production") {
      currentVersion = require('./package.json').version;
    } else {
      currentVersion = globalInfo.appVersion;
    }

    return currentVersion;
  }
  //* endof вспомогательные функции, НЕ ОБРАБОТЧИКИ


  //* обработчики
  function goBack(e) {
    history.goBack();
  }
  //* endof обработчики


  return (
    <div className="">
      <TopBar backOption={true} title={title} />
      <div className="d-flex w-100 w-100 h-maxcontent bg-gray1">
        <div className="bg-white w-25 my-4 mr-5">
          <h1 className="text-uppercase size-36 weight-700">Помощь</h1>
          <p>Система "Аквакультура" для ускорения учета икры на предприятии.</p>
          <p>Версия {getCurrentAppVersion()}</p>
          <p>
            <a className="link ml-2" href={instructionByUser()}>
              Руководство пользователя
            </a>
            <br />

            <br />
          </p>

        </div>
      </div>
    </div>
  );
}
