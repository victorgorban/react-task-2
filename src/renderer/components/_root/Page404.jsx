import  React, { useEffect, useState, useRef } from "react";
import { useHistory } from "react-router-dom";


export default function Page404() {
  //* библиотеки и неизменяемые значения 
  const history = useHistory();
  //* endof библиотеки и неизменяемые значения 


  //* эффекты
  useEffect(() => {
    document.title = "Страница не найдена"
}, [])
  //* endof эффекты

  
  //* обработчики
  function goBack(e) {
    history.goBack()
}
  //* endof обработчики

    return (
        <div className=''>
            <div className="">
                <div className="header mt-2">
                    страница не найдена :(
                </div>
                <div className="">
                    страница: <code>{history.location.pathname}</code>
                </div>

                <div className="subheader mt-2">
                    Возможно, она была перемещена или удалена
                </div>

                <div type="button" tabIndex="-1" className="btn gray mt-3" onClick={goBack}>
                ← Вернуться назад
                </div>
            </div>
        </div>
    )
}