import qrcodePurpleIcon from "@assets/img/icons/qrcode-purple.svg";
import qrcodeGrayIcon from "@assets/img/icons/qrcode-gray.svg";

import React, { useEffect, useState, useRef } from "react";
import { useHistory } from "react-router-dom";

import { useStore } from 'effector-react'
import { $loginData } from '@renderer/stores/global'
import { $userData } from '@renderer/stores/global'

import TopBar from '@components/_root/TopBar'
import { redirectLink } from '@renderer/helpers'

import * as helpers from '@renderer/helpers'
import * as notifications from '@renderer/notifications'

export default function Login({ }) {
  //* библиотеки и неизменяемые значения 

  let elRef = useRef();
  const history = useHistory();
  let title = "Вход в систему"

  //* endof библиотеки и неизменяемые значения 


  //* глобальное состояние из store
  let loginData = useStore($loginData)
  let userData = useStore($userData)
  let loggedIn = !!userData // window.userData будет реактивная переменная как из useState, когда я сделаю логин
  //* endof глобальное состояние из store


  //* состояние
  //* endof состояние


  //* вычисляемые переменные, изменение состояния

  //* endof вычисляемые переменные, изменение состояния


  //* эффекты
  useEffect(() => {
    document.title = title;
  }, [])

  //* endof эффекты


  //* вспомогательные функции, НЕ ОБРАБОТЧИКИ

  //* endof вспомогательные функции, НЕ ОБРАБОТЧИКИ


  //* обработчики
  async function handleScanUser(scannedString) {
    let roleCode = scannedString.substr(0, 2);
    $userData.api.replace({ role: roleCode, barcode: scannedString })

    switch (roleCode) {
      case '00':
        {
          history.push('/menu')
          break;
        }
      case '01':
        {
          history.push('/mode_tech/main')
          break;
        }
      case '02':
        {
          history.push('/mode_packer/menu')
          break;
        }
      case '03':
        {
          history.push('/mode_warehouser/menu')
          break;
        }
      default: {
        notifications.showUserError(`Неизвестный код роли пользователя: ${roleCode}`)
        break;
      }
    }
  }

  //* endof обработчики

  return (
    <label htmlFor="scanInput" className="w-100" ref={elRef}>
      <TopBar backOption={false} title={title} />
      <div className="d-flex w-100 w-100 h-maxcontent bg-gray1">
        <div className="w-66 h-100 d-flex all-center">
          <div
            className="d-inline-flex flex-column align-items-center justify-content-center py-5 bg-gray3 color-purple border-purple border-dashed"
            style={{ width: '600px', height: '350px' }}
          >
            <img src={qrcodePurpleIcon} className="mb-4" alt="" style={{ width: '136px', height: 'auto' }} />

            <div className="text-center focus size-24 weight-500">Для входа в систему отканируйте код карты работника</div>
            <input id="scanInput" autoFocus
              className="visually-hidden" type="text"
              onKeyDown={(e) => { if (e.key === 'Enter') helpers.handleEnterScan(e, handleScanUser) }} />
          </div>
        </div>

        <div className="w-33 h-100">
          <div className="h-100 w-100 bg-black overflow-hidden">
            <img src={qrcodeGrayIcon} alt="" style={{ width: '560px', height: 'auto', position: 'absolute', bottom: '0', right: '0' }} />
          </div>
        </div>
      </div>

    </label>
  );
}
