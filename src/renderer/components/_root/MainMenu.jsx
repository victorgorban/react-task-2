import menuIconTech from "@assets/img/icons/tech-green.svg";
import menuIconPacker from "@assets/img/icons/packer-green.svg";
import menuIconPumping from "@assets/img/icons/pumping-green.svg";
import menuIconWarehouser from "@assets/img/icons/warehouser-green.svg";

import React, { useEffect, useState, useRef } from "react";
import { useHistory } from "react-router-dom";

import { useStore } from 'effector-react'
import { $loginData } from '@renderer/stores/global'
import { $userData} from '@renderer/stores/global'

import TopBar from '@components/_root/TopBar'
import { redirectLink } from '@renderer/helpers'

import * as helpers from '@renderer/helpers'
import * as notifications from '@renderer/notifications'

export default function MainMenu({ }) {
  //* библиотеки и неизменяемые значения 

  let elRef = useRef();
  const history = useHistory();
  let title = "Меню (выбор режима работы)"

  //* endof библиотеки и неизменяемые значения 


  //* глобальное состояние из store
  let loginData = useStore($loginData)
  let userData = useStore($userData)
  let loggedIn = !!userData // window.userData будет реактивная переменная как из useState, когда я сделаю логин
  //* endof глобальное состояние из store


  //* состояние
  //* endof состояние


  //* вычисляемые переменные, изменение состояния

  //* endof вычисляемые переменные, изменение состояния


  //* эффекты
  useEffect(() => {
    document.title = title;
  }, [])

  //* endof эффекты


  //* вспомогательные функции, НЕ ОБРАБОТЧИКИ

  //* endof вспомогательные функции, НЕ ОБРАБОТЧИКИ


  //* обработчики
  async function handleScanUser(scannedString) {
    let roleCode = scannedString.substr(0, 2);
    $userData.api.replace({ role: roleCode, barcode: scannedString })

    switch (roleCode) {
      case '00':
        {
          history.push('/menu')
          break;
        }
      case '01':
        {
          history.push('/mode_tech/main')
          break;
        }
      /* case '02':
        {
          history.push('/mode_packer/menu')
          break;
        }
      case '03':
        {
          history.push('/mode_warehouser/menu')
          break;
        } */
        default: {
          notifications.showUserError(`Неизвестный код роли пользователя: ${roleCode}`)
          break;
        }
    }
  }

  //* endof обработчики

  return (
    <label htmlFor="scanInput" className="w-100" ref={elRef}>
      <TopBar backOption={false} title={title} />
      <div className="d-flex w-100 w-100 h-maxcontent p-4 bg-gray1">
        <div className="w-33 h-50 p-1">
          <div
            className="w-100 h-100 menu-item btn d-inline-flex flex-column align-items-center justify-content-between py-5 border-green color-green"
            onClick={e => redirectLink('/mode_tech/main', history)}
          >
            <img src={menuIconTech} alt="" style={{ width: '40%', height: 'auto' }} />
            <div className="text-uppercase size-24 weight-700 text-center">Технолог</div>
          </div>
        </div>

        {/* <div className="w-33 h-50 p-1">
          <div
            className="w-100 h-100 menu-item btn d-inline-flex flex-column align-items-center justify-content-between py-5 border-green color-green"
            onClick={e => redirectLink('/mode_packer/menu', history)}
          >
            <img src={menuIconPacker} alt="" style={{ width: '40%', height: 'auto' }} />
            <div className="text-uppercase size-24 weight-700 text-center">Фасовщик</div>
          </div>
        </div>

        <div className="w-33 h-50 p-1">
          <div
            className="w-100 h-100 menu-item btn d-inline-flex flex-column align-items-center justify-content-between py-4 border-green color-green"
            onClick={e => redirectLink('/mode_pumper/main', history)}
          >
            <img src={menuIconPumping} className="p-3" alt="" style={{ width: '40%', height: 'auto' }} />
            <div className="text-uppercase size-24 weight-700 text-center">Сцеживание</div>
          </div>
        </div> */}


        <input id="scanInput" autoFocus
          className="visually-hidden" type="text"
          onKeyDown={(e) => { if (e.key === 'Enter') helpers.handleEnterScan(e, handleScanUser) }} />

      </div>

    </label>
  );
}
