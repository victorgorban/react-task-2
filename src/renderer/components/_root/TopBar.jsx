import defaultUserImg from "@assets/img/icons/user-avatar-black.svg";
import arrowLeft from "@assets/img/icons/arrow-left-white.svg";
import infoIconWhite from '@assets/img/icons/info-white.svg'
import infoIconPurple from '@assets/img/icons/info-purple.svg'
import logoutIconWhite from '@assets/img/icons/logout-white.svg'

import React, { useEffect, useState, useRef } from "react";
import { useHistory } from "react-router-dom";
import UnopDropdown from "unop-react-dropdown";

import { useStore } from 'effector-react'
import { $loginData } from '@renderer/stores/global'
import { $userData} from '@renderer/stores/global'
import { $requestsPending } from '@renderer/stores/global'
import * as helpers from '@renderer/helpers'

const { ipcRenderer } = require("electron");

export default function Topbar({ backOption = true, backText = "Вернуться назад", title = "Без названия" }) {
  //* библиотеки и неизменяемые значения 

  window.logout = logout;
  let elRef = useRef();
  const history = useHistory();
  //* endof библиотеки и неизменяемые значения 


  //* глобальное состояние из store
  let requestsPending = useStore($requestsPending)
  let userData = useStore($userData)
  let loggedIn = !!userData // window.userData будет реактивная переменная как из useState, когда я сделаю логин
  //* endof глобальное состояние из store


  //* состояние
  //* endof состояние


  //* вычисляемые переменные, изменение состояния

  //* endof вычисляемые переменные, изменение состояния


  //* Переменные для всего компонента

  //* endof Переменные для всего компонента


  //* эффекты

  //* endof эффекты


  //* вспомогательные функции, НЕ ОБРАБОТЧИКИ

  //* endof вспомогательные функции, НЕ ОБРАБОТЧИКИ


  //* обработчики

  function logout(e) {
    e?.preventDefault();
    e?.stopPropagation();
    localStorage.removeItem('loginData');
    localStorage.removeItem('userData');

    sessionStorage.removeItem('loginData');
    sessionStorage.removeItem('userData');

    $loginData.api.reset()
    $userData.api.reset()

    history.push('/')
  }

  function exit(e) {
    e?.preventDefault();
    e?.stopPropagation();

    ipcRenderer.send('exit_from_renderer')
  }

  //* endof обработчики

  return (
    <div className="bg-black w-100" style={{ height: '100px' }} ref={elRef}>
      <div className="h-100 d-flex flex-row align-items-center">
        <div className="w-25 h-100 d-flex flex-row">
          {backOption &&
            <div
              className="h-100 btn d-flex all-center bg-gray color-white size-24 weight-600 mr-1"
              style={{ width: '115px' }}
              onClick={e => helpers.goBack(history)}>
              <img src={arrowLeft} style={{ width: '30px', height: 'auto' }} alt="" />
            </div>
          }

          <div
            className="h-100 btn flex-grow-1 d-flex all-center bg-yellow size-24 weight-600"
            onClick={e => history.push('/menu')}>
            В главное меню
          </div>
        </div>

        <div className="h-100 d-flex align-items-center flex-grow-1 text-uppercase color-white px-5 size-30 weight-700">
          {/* {requestsPending} */}
          <div className={`h-100 mr-2 d-inline-flex all-center ${requestsPending && 'loading'}`} style={{ width: '40px' }}>
            <div className="loader lds-ring color-purple" style={{width: '40px', height: '40px'}}>
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>
          {title || 'Без названия'}
        </div>

        <div className="h-100 w-25">
          {loggedIn &&
            <div className="h-100 profile-block-wrapper d-flex flex-row all-center bg-black color-white px-4">
              <UnopDropdown
                align="LEFT"
                trigger={
                  <div className="profile-block d-flex flex-row all-center bg-black color-white px-4" style={{ width: '405px' }}>
                    <img className="avatar" style={{ width: '56px', height: '56px' }} src={defaultUserImg} alt="" />
                    <div className="d-flex flex-column justify-content-center ml-2">
                      <div className="user-name size-18 weight-700 text-uppercase">{userData.name || 'Без имени'}</div>
                      <div className="user-role size-14 weight-500">[ код {userData.barcode || 'Без кода'} ] {userData.role || 'Без роли'}</div>
                    </div>
                  </div>
                }>
                {/* w-100 не работает по причине перезаписи стилей в либе */}
                <div className="w-100 d-flex flex-column bg-white pt-1" style={{ width: '405px' }}>
                  <div className="btn w-100 bg-green color-white d-flex flex-row justify-content-start align-items-center"
                    style={{ height: '90px' }}
                    onClick={e => history.push('/help')}>
                    <img src={infoIconWhite} className="ml-5" alt="" style={{ width: '30px', height: 'auto' }} />
                    <span className="size-18 weight-700 ml-3">Помощь</span>
                  </div>
                  <div className="btn w-100 bg-yellow color-black d-flex flex-row justify-content-start align-items-center mt-1"
                    style={{ height: '90px' }}
                    onClick={e => history.push('/settings')}>
                    <img src={infoIconPurple} className="ml-5" alt="" style={{ width: '30px', height: 'auto' }} />
                    <span className="size-18 weight-700 ml-3">Настройки</span>
                  </div>
                  <div className="btn w-100 bg-black color-white d-flex flex-row justify-content-start align-items-center mt-1"
                    style={{ height: '90px' }}
                    onClick={e => logout(e)}>
                    <img src={logoutIconWhite} className="ml-5" alt="" style={{ width: '30px', height: 'auto' }} />
                    <span className="size-18 weight-700 ml-3">Выйти из системы</span>
                  </div>
                  <div className="btn w-100 bg-black color-white d-flex flex-row justify-content-start align-items-center mt-1"
                    style={{ height: '90px' }}
                    onClick={e => exit(e)}>
                    <img src={logoutIconWhite} className="ml-5" alt="" style={{ width: '30px', height: 'auto' }} />
                    <span className="size-18 weight-700 ml-3">Закрыть приложение</span>
                  </div>
                </div>

              </UnopDropdown>
            </div>
          }
        </div>
      </div>
    </div>
  );
}
