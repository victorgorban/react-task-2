import menuIconTech from "@assets/img/icons/tech-green.svg";
import menuIconPacker from "@assets/img/icons/packer-green.svg";
import menuIconWarehouser from "@assets/img/icons/warehouser-green.svg";

import React, { useEffect, useState, useRef } from "react";
import { useHistory } from "react-router-dom";

import { useStore } from 'effector-react'
import { $loginData } from '@renderer/stores/global'
import { $userData} from '@renderer/stores/global'
import { $globalSettings, setGlobalSettings } from '@renderer/stores/global'


import _ from 'lodash'
import * as helpers from '@renderer/helpers'

import TopBar from '@components/_root/TopBar'
import { redirectLink } from '@renderer/helpers'

export default function Settings({ }) {
  //* библиотеки и неизменяемые значения 

  let elRef = useRef();
  const history = useHistory();
  let title = "Настройки приложения"

  //* endof библиотеки и неизменяемые значения 


  //* глобальное состояние из store
  let globalSettings = useStore($globalSettings)
  let userData = useStore($userData)
  let loggedIn = !!userData // window.userData будет реактивная переменная как из useState, когда я сделаю логин
  //* endof глобальное состояние из store


  //* состояние
  //* endof состояние


  //* вычисляемые переменные, изменение состояния

  //* endof вычисляемые переменные, изменение состояния


  //* эффекты
  useEffect(() => {
    document.title = title;
  }, [])

  //* endof эффекты


  //* вспомогательные функции, НЕ ОБРАБОТЧИКИ

  //* endof вспомогательные функции, НЕ ОБРАБОТЧИКИ


  //* обработчики
  async function setGlobalSetting(e, path, type) {
    let value = e.currentTarget.value;

    if (type == "float") {
      value = parseFloat(value);
    } else if (type == "int") {
      value = parseInt(value);
    } else if (type == "bool") {
      value = value == "true";
    } else if (type == "bool-null") {
      value = value == "null" ? null : value == "true";
    } else if (type == "date") {
      value = helpers.parseDate(value);
      value = helpers.formatDate(value, "YYYY-MM-DD")
    } else {
      // value = value;
    }

    _.set(globalSettings, path, value)
    setGlobalSettings({ ...globalSettings })
  }

  //* endof обработчики

  return (
    <div className="" ref={elRef}>
      <TopBar backOption={true} title={title} />
      <div className="d-flex w-100 w-100 h-maxcontent p-4 bg-gray1">
        <div className="w-33 h-100 mx-3 p-3 bg-white">
          <div
            className="w-100 h-100 d-inline-flex flex-column justify-content-start"

          >
            <div className="w-100">
              <span className="w-100 size-36 weight-700 text-center">Печать <span className="size-16 weight-500">(настройки сохраняются автоматически)</span></span>
            </div>

            <div className="w-100 d-flex justify-content-between align-items-end mt-5">
              <span className="size-24 weight-700">Размер кода для сырья (мм)</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="number"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.raw?.height || ''}
                  onChange={e => setGlobalSetting(e, 'print.raw.height', 'int')}
                  placeholder="высота"
                />

                <input type="number"
                  // disabled
                  className="ml-1"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.raw?.width || ''}
                  onChange={e => setGlobalSetting(e, 'print.raw.width')}
                  placeholder="ширина"
                />

              </span>
            </div>

            <div className="w-100 d-flex justify-content-between align-items-end mt-1">
              <span className="size-24 weight-700">Отступ кода для сырья (мм)</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="number"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.raw?.padding || ''}
                  onChange={e => setGlobalSetting(e, 'print.raw.padding', 'int')}
                  placeholder=""
                />

              </span>
            </div>

            <div className="w-100 d-flex justify-content-between align-items-end mt-1">
              <span className="size-24 weight-700">Формат кода для сырья</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="text"
                  style={{ width: '210px' }}
                  placeholder="code128"
                  value={globalSettings.print?.raw?.bcid || ''}
                  onChange={e => setGlobalSetting(e, 'print.raw.bcid')}
                />

              </span>
            </div>


            <div className="w-100 d-flex justify-content-between align-items-end mt-5">
              <span className="size-24 weight-700">Размер кода для товаров (мм)</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="number"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.item?.height || ''}
                  onChange={e => setGlobalSetting(e, 'print.item.height', 'int')}
                  placeholder="высота"
                />

                <input type="number"
                  // disabled
                  className="ml-1"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.item?.width || ''}
                  onChange={e => setGlobalSetting(e, 'print.item.width')}
                  placeholder="ширина"
                />

              </span>
            </div>

            <div className="w-100 d-flex justify-content-between align-items-end mt-1">
              <span className="size-24 weight-700">Отступ кода для товаров (мм)</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="number"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.item?.padding || ''}
                  onChange={e => setGlobalSetting(e, 'print.item.padding', 'int')}
                  placeholder=""
                />

              </span>
            </div>

            <div className="w-100 d-flex justify-content-between align-items-end mt-1">
              <span className="size-24 weight-700">Формат кода для товаров</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="text"
                  style={{ width: '210px' }}
                  placeholder="qrcode"
                  value={globalSettings.print?.item?.bcid || ''}
                  onChange={e => setGlobalSetting(e, 'print.item.bcid')}
                />

              </span>
            </div>


            <div className="w-100 d-flex justify-content-between align-items-end mt-5">
              <span className="size-24 weight-700">Размер кода для партий (мм)</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="number"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.batch?.height || ''}
                  onChange={e => setGlobalSetting(e, 'print.batch.height', 'int')}
                  placeholder="высота"
                />

                <input type="number"
                  // disabled
                  className="ml-1"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.batch?.width || ''}
                  onChange={e => setGlobalSetting(e, 'print.batch.width')}
                  placeholder="ширина"
                />

              </span>
            </div>

            <div className="w-100 d-flex justify-content-between align-items-end mt-1">
              <span className="size-24 weight-700">Отступ кода для партий (мм)</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="number"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.batch?.padding || ''}
                  onChange={e => setGlobalSetting(e, 'print.batch.padding', 'int')}
                  placeholder=""
                />

              </span>
            </div>

            <div className="w-100 d-flex justify-content-between align-items-end mt-1">
              <span className="size-24 weight-700">Формат кода для партий</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="text"
                  style={{ width: '210px' }}
                  placeholder="code128"
                  value={globalSettings.print?.batch?.bcid || ''}
                  onChange={e => setGlobalSetting(e, 'print.batch.bcid')}
                />

              </span>
            </div>


            {/* <div className="w-100 d-flex justify-content-between align-items-end mt-1">
              <span className="size-24 weight-700">Размер страницы</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="text"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.page?.height || ''}
                  onChange={e => setGlobalSetting(e, 'print.page.height')}
                  placeholder="height"
                />

                <input type="text"
                  className="ml-1"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.page?.width || ''}
                  onChange={e => setGlobalSetting(e, 'print.page.width')}
                  placeholder="width"
                />

              </span>
            </div> */}


          </div>
        </div>

        <div className="w-33 h-100 mx-3 p-3 bg-white">
          {/* <div
            className="w-100 h-100 d-inline-flex flex-column justify-content-start"

          >
            <div className="w-100">
              <span className="w-100 size-36 weight-700 text-center">Печать</span>
            </div>

            <div className="w-100 d-flex justify-content-between align-items-end mt-1">
              <span className="size-24 weight-700">Размер штрих-кода</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="text"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.raw?.height || ''}
                  onChange={e => setGlobalSetting(e, 'print.raw.height')}
                  placeholder="height"
                />

                <input type="text"
                  className="ml-1"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.raw?.width || ''}
                  onChange={e => setGlobalSetting(e, 'print.raw.width')}
                  placeholder="width"
                />

              </span>
            </div>

            <div className="w-100 d-flex justify-content-between align-items-end mt-1">
              <span className="size-24 weight-700">Размер QR-кода</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="text"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.qrcode?.height || ''}
                  onChange={e => setGlobalSetting(e, 'print.qrcode.height')}
                  placeholder="height"
                />

                <input type="text"
                  className="ml-1"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.qrcode?.width || ''}
                  onChange={e => setGlobalSetting(e, 'print.qrcode.width')}
                  placeholder="width"
                />

              </span>
            </div>


          </div> */}
        </div>

        <div className="w-33 h-100 mx-3 p-3 bg-white">
          {/* <div
            className="w-100 h-100 d-inline-flex flex-column justify-content-start"

          >
            <div className="w-100">
              <span className="w-100 size-36 weight-700 text-center">Печать</span>
            </div>

            <div className="w-100 d-flex justify-content-between align-items-end mt-1">
              <span className="size-24 weight-700">Размер штрих-кода</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="text"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.raw?.height || ''}
                  onChange={e => setGlobalSetting(e, 'print.raw.height')}
                  placeholder="height"
                />

                <input type="text"
                  className="ml-1"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.raw?.width || ''}
                  onChange={e => setGlobalSetting(e, 'print.raw.width')}
                  placeholder="width"
                />

              </span>
            </div>

            <div className="w-100 d-flex justify-content-between align-items-end mt-1">
              <span className="size-24 weight-700">Размер QR-кода</span>
              <div className="flex-grow-1 mx-1" style={{ borderBottom: '1px dashed var(--gray2)' }}></div>
              <span className="d-inline-flex size-24 weight-600">
                <input type="text"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.qrcode?.height || ''}
                  onChange={e => setGlobalSetting(e, 'print.qrcode.height')}
                  placeholder="height"
                />

                <input type="text"
                  className="ml-1"
                  style={{ width: '100px' }}
                  value={globalSettings.print?.qrcode?.width || ''}
                  onChange={e => setGlobalSetting(e, 'print.qrcode.width')}
                  placeholder="width"
                />

              </span>
            </div>


          </div> */}
        </div>




      </div>

    </div>
  );
}
