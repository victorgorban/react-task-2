import bwipjs from 'bwip-js'
import delay from 'delay'
import React, { useEffect, useState, useRef } from "react";

import { useHistory } from "react-router-dom";

import { useStore } from 'effector-react'
import { $userData } from '@renderer/stores/global'
import { $globalSettings } from '@renderer/stores/global'
import { $rawCategories } from '@renderer/stores/global'
import { $process } from '@renderer/stores/global'

import * as helpers from '@renderer/helpers'

import TopBar from '@components/_root/TopBar'

import qrcodeIcon from '@assets/img/icons/qrcode-purple.svg'
import crossWhiteIcon from '@assets/img/icons/cross-white.svg'
import infoIcon from '@assets/img/icons/info-purple.svg'
import arrowLeftBlack from '@assets/img/icons/arrow-left-black.svg'

import * as notifications from '@renderer/notifications'

import _ from "lodash"

// const { ipcRenderer } = require("electron");
let batchContentItemRender = 0

export default function TechMain({ }) {
  //* библиотеки и неизменяемые значения 
  let elRef = useRef();
  const history = useHistory();
  let title = "Техпроцесс"

  //* endof библиотеки и неизменяемые значения 


  //* глобальное состояние из store
  let globalSettings = useStore($globalSettings)
  let rawCategories = useStore($rawCategories)
  let allOperations = useStore($process)
  let userData = useStore($userData)
  //* endof глобальное состояние из store


  //* состояние
  let [isLoading, setLoading] = useState(false);

  let [batchInfo, setBatchInfo] = useState({})
  let [batchContent, setBatchContent] = useState([])
  let [selectedBatchContent, setSelectedBatchContent] = useState([])
  let [product, setProduct] = useState({})
  let [currentMode, setCurrentMode] = useState(null)
  let [selectedOperation, setSelectedOperation] = useState(null)
  let [fishDatas, setFishDatas] = useState([])
  let [isWeightChecked, setWeightChecked] = useState(false)
  let [isWeightCorrect, setWeightCorrect] = useState(null)
  let [isBatchLoading, setBatchLoading] = useState(false)
  let [correctedWeight, setCorrectedWeight] = useState(0)

  let [preparedCategories, setPreparedCategories] = useState([])

  // todo этот блок надо разбить на компоненты меню
  let [selectedCatTab, setSelectedCatTab] = useState('group')
  let [selectedCatGroup, setSelectedCatGroup] = useState('')
  let [selectedCatGrade, setSelectedCatGrade] = useState('')
  let [selectedCatNode, setSelectedCatNode] = useState(null)

  let [weightAggregation, setWeightAggregation] = useState(0)
  let [weightPacker, setWeightPacker] = useState(0)
  let [weightsDivision, setWeightsDivision] = useState([0, 0])
  let [weightOperations, setWeightOperations] = useState(0)

  //* endof состояние

  //* вычисляемые переменные, изменение состояния
  selectedBatchContent = _.sortBy(selectedBatchContent, 'packet_number')
  let batchContentNotPrinted = batchContent.filter(item => !item.printed)
  let selectedBatchContentNotPrinted = selectedBatchContent.filter(item => !item.printed)
  let sumBatchWeight = +batchContent.reduce((sum, item) => sum + item.weight, 0).toFixed(2)
  let sumSelectedBatchContentsWeight = +selectedBatchContent.reduce((sum, item) => sum + item.weight, 0).toFixed(2)

  let isFirstCatTab = selectedCatTab == 'group'

  const allGrades = [{ name: '1 сорт' }, { name: '2 сорт' }, { name: '3 сорт' }]
  const allModes = [
    { title: 'Операции', name: 'operations', isDisabled: !selectedBatchContentNotPrinted.length },
    { title: 'Агрегация', name: 'aggregation', isDisabled: selectedBatchContentNotPrinted.length < 2 },
    { title: 'Разделение', name: 'division', isDisabled: selectedBatchContentNotPrinted.length != 1 },
    { title: 'Выбор рецепта', name: 'recipe', isDisabled: !selectedBatchContentNotPrinted.length },
    { title: 'Отправка на фасовку', name: 'packer', isDisabled: !selectedBatchContentNotPrinted.length || selectedBatchContentNotPrinted.find(raw => !raw.cat) } // у всех задан рецепт-категория
  ]

  //* endof вычисляемые переменные, изменение состояния


  //* эффекты
  useEffect(() => {
    document.title = title;
  }, [])

  useEffect(() => {
    if (!rawCategories.length) return;
    let preparedCategories = _.cloneDeep(rawCategories)

    for (let item of Object.values(preparedCategories)) {
      item.grades = _.cloneDeep(allGrades);
      for (let grade of item.grades) {
        grade.parent = item;
      }
    }

    setPreparedCategories(preparedCategories)
  }, [rawCategories])


  useEffect(async () => {
    if (!batchContent.length) return;

    // Фильтруем те id рыб, которых еще у нас нет
    let fishIdsToGet = _.uniq(batchContent.map(item => item.fish)).filter(fish => fish && !fishDatas.find(fd => fd._id == fish))

    if (!fishIdsToGet.length) return;
    try {
      let result = await helpers.submitObject("getFishDataBatch", { fids: fishIdsToGet })
      fishDatas.push(...result.data)
      setFishDatas([...fishDatas])
    } catch (e) {
      notifications.showSystemError(e.message || e)
      console.log(e);
    }

  }, [batchContent])

  //* endof эффекты


  //* вспомогательные функции, НЕ ОБРАБОТЧИКИ
  function fishName(fishId) {
    return fishDatas.find(item => item._id == fishId)?.name || ''
  }

  function categoryName(cat) {
    if (!cat) return null
    return `${cat.group || ''} ${cat.grade || ''}`
  }

  //* endof вспомогательные функции, НЕ ОБРАБОТЧИКИ


  //* обработчики
  // выставление режима работы. null - изначальный экран, ничего не выбрано
  function handleSetMode(newMode) {
    setCurrentMode(newMode)
    if (newMode == null) {
      setWeightsDivision([0, 0])
      setWeightAggregation(0)
      setWeightPacker(0)
      setWeightOperations(0)
    }
  }

  function handleScanWeightAggregation(weightString) {
    let weight = +weightString.replace(',', '.')
    setWeightAggregation(weight)
  }

  function handleScanWeightDivision(weightString) {
    let weight = +weightString.replace(',', '.')
    // console.log('weightsDivision in scanWeight', weightsDivision, window.weightsDivision);
    let currentWeightIndex = weightsDivision.findIndex(weight => !weight);
    // console.log('currentWeightIndex', currentWeightIndex);
    if (currentWeightIndex == -1) {
      notifications.showUserError('Все веса уже заполнены')
      return;
    }
    weightsDivision[currentWeightIndex] = weight
    setWeightsDivision([...weightsDivision])
  }

  function handleScanWeightPacker(weightString) {
    let weight = +weightString.replace(',', '.')
    setWeightPacker(weight)
  }

  function handleScanWeightOperations(weightString) {
    let weight = +weightString.replace(',', '.')
    setWeightOperations(weight)
  }

  // Выбрали/отменили выбор пакета
  function toggleSelectItem(item, idx) {
    let foundIndex = selectedBatchContent.indexOf(item)
    if (foundIndex != -1) { // found
      selectedBatchContent.splice(foundIndex, 1)
    } else {
      item.packet_number = idx + 1
      selectedBatchContent.push(item)
    }

    setSelectedBatchContent([...selectedBatchContent])
  }

  // Выбрали очередной уровень категории. Сейчас их только 2, group и grade, но в будущем планируется еще минимум 2
  async function selectCatNode(e, node, tab) {
    /* e?.preventDefault();
    e?.stopPropagation(); */

    if (tab == 'group') {
      setSelectedCatGroup(node.name)
    } else if (tab == 'grade') {
      setSelectedCatGrade(node.name)
    }

    if (tab != 'grade') {
      setSelectedCatNode(node);
    }

    if (tab == 'grade') {
      let myCat = preparedCategories.find(item => item.name == selectedCatGroup)
      product.cat = {
        id: myCat?.id,
        group: selectedCatGroup,
        grade: node.name,
      }

      let promises = []
      try {
        /* let rawsToChange = selectedBatchContent
        if (!selectedBatchContent.length) {
          rawsToChange = batchContent;
        } */
        for (let raw of selectedBatchContent) {
          promises.push(helpers.submitObject('setRawCat', { rawId: raw._id, cat: product.cat, userId: userData.barcode || 'admin' }))
          raw.cat = product.cat;
        }


        await Promise.all(promises);
        // console.log('rawsToChange', rawsToChange);
        setBatchContent([...batchContent])
        setSelectedBatchContent([...selectedBatchContent])
      } catch (e) {
        notifications.showSystemError(e.message || e)
        console.log(e);
      }


      setProduct({ ...product })
      setSelectedCatTab('group')

      setSelectedCatGroup('')
      setSelectedCatGrade('')
      setSelectedCatNode(null)
      handleSetMode(null)
      return;
    }

    if (node.cat === true) { // если у категории нет детей, то выбираем уровень качества
      setSelectedCatTab('grade');
      return
    }

  }

  // Нажали "Назад" в выборе категории
  function selectParentCatNode(e) {
    /* e?.preventDefault();
    e?.stopPropagation(); */

    let node = selectedCatNode;

    let parentNode = node.parent || {}
    setSelectedCatNode(parentNode);
    if (!parentNode.parent) { // у группы нет родителя
      setSelectedCatTab('group');
      return
    }
  }

  // Нажали "вес верный" или "вес неверный"
  async function handleCheckBatchWeight(isCorrect) {
    try {
      setWeightChecked(true)
      setWeightCorrect(isCorrect)

      if (!isCorrect) {
        setCorrectedWeight(sumBatchWeight)
        return;
      }
      // отправка идет только при подтверждении веса

      let params = { batchId: batchInfo.batch._id, approve: isCorrect }
      if (correctedWeight) {
        params.newWeight = correctedWeight
      }
      let result = await helpers.submitObject("approveBatchWeight", params)

    } catch (e) {
      notifications.showSystemError(e.message || e)
      console.error(e)
      return
    }
  }

  function handleCancelWeightCorrecting() {
    setWeightCorrect(null);
    setWeightChecked(false);
    setCorrectedWeight(0);
  }

  // Отсканировали код партии
  async function handleScanBatch(scannedString) {
    try {
      if (batchContent.filter(item => item.printed).length != batchContent.length) {
        notifications.showUserError('Сначала закончите работу с текущей партией')
        return;
      }

      setBatchLoading(true)

      let result = await helpers.submitObject("getBatchWithContent", { barcode: scannedString })

      setBatchInfo(result.data)
      setBatchContent(result.data.content || [])
      setProduct({})
      setBatchLoading(false)

      setWeightChecked(false)
      setWeightCorrect(null)
      setSelectedBatchContent([])
      setCorrectedWeight(0)


      let generateOptions = {
        bcid: 'code128',
        text: result.data.batch.barcode,
        scale: 3,
        height: 30,              // Bar height, in millimeters
        width: 90,
        padding: 0,
        includetext: false,            // Show human-readable text
        textxalign: 'center',        // Always good to set this
      }

      bwipjs.toCanvas('scanCanvas', generateOptions)

    } catch (e) {
      notifications.showSystemError(e.message || e)
      console.error(e)
      setBatchLoading(false)
      return
    }
  }

  // Отсканировали код сырья
  async function handleScanRaw(scannedString) {
    try {
      let result = await helpers.submitObject("getRawContent", { barcode: scannedString })

      let newContent = result.data;
      if (batchContent.find(content => content._id == newContent._id)) {
        notifications.showUserError('Этот пакет уже добавлен')
        return;
      }

      console.log('newContent._id', newContent._id);

      setBatchContent(prev => [...prev, newContent])

      setSelectedBatchContent(prev => [...prev, newContent])
    } catch (e) {
      notifications.showSystemError(e.message || e)
      console.error(e)
      return
    }
  }

  // Отсканировали какой-то код
  async function handleScan(scannedString) {
    // Если строка начинается с w, то это ввод веса
    if (scannedString.substr(0, 1) == 'w') {
      let weightStr = scannedString.substr(1)
      switch (currentMode?.name) {
        case 'aggregation': {
          handleScanWeightAggregation(weightStr)
          break;
        }
        case 'division': {
          handleScanWeightDivision(weightStr)
          break;
        }
        case 'packer': {
          handleScanWeightPacker(weightStr)
          break;
        }
        case 'operations': {
          handleScanWeightOperations(weightStr)
          break;
        }

        default: {
          break;
        }
      }
      // Код партии содержит точки
    } else if (scannedString.includes('.')) { handleScanBatch(scannedString) }
    else {
      // Код пакета с сырьем не содержит точки и не начинается с w
      handleScanRaw(scannedString)
    }
  }

  // Применить выбранную операцию
  async function makeOperation() {
    if (!selectedBatchContentNotPrinted.length) {
      notifications.showUserError('Пакет не выбран')
      return
    }

    if (selectedBatchContentNotPrinted.length > 1) {
      notifications.showUserError('Нужно выбрать ровно 1 пакет')
      return
    }
    try {
      setLoading(true)
      let itemToApply = selectedBatchContentNotPrinted[0]
      let result = await helpers.submitObject('setRawProcess', { rawId: itemToApply._id, process: selectedOperation, weight: weightOperations, userId: userData.barcode || 'admin' })
      itemToApply.process = { ...selectedOperation, ...result.data }
      itemToApply.weight = weightOperations
      setBatchContent([...batchContent])
      setSelectedBatchContent([])
      handleSetMode(null)
    } catch (e) {
      notifications.showSystemError(e.message || e)
      console.log(e)
    } finally {
      setLoading(false)
      setWeightOperations(0)
    }
  }

  // Отправить пакеты фасовщику
  async function sendToPacker() {
    if (!selectedBatchContent.length) {
      notifications.showUserError('Пакеты не выбраны')
      return;
    }

    for (let item of selectedBatchContent) {
      item.printed = true;
    }

    setWeightPacker(0)

    setBatchContent([...batchContent])
    setSelectedBatchContent([...selectedBatchContent])
  }

  // Добавить вес разделяемой части для разделения пакета
  function addWeightsDivision() {
    weightsDivision.push(0)
    setWeightsDivision([...weightsDivision])
  }

  // Разделить пакет
  async function makeDivision() {
    if (!selectedBatchContent.length) {
      notifications.showUserError('Пакеты не выбраны')
      return;
    }

    if (selectedBatchContent.length > 1) {
      notifications.showUserError('Нужно выбрать ровно 1 пакет')
      return;
    }

    try {
      setLoading(true);

      let itemToDivide = selectedBatchContent[0];

      let result = await helpers.submitObject("makeRawsFromRaw", { rawId: itemToDivide._id, weights: weightsDivision, userId: userData.barcode || 'admin' })

      // console.log('division result', result);

      let newItems = result.data.raws.map((rawId, idx) => ({
        ...itemToDivide,
        _id: rawId,
        weight: weightsDivision[idx],
        date: new Date()
      }))

      let itemIndex = batchContent.indexOf(itemToDivide)
      batchContent.splice(itemIndex, 1, ...newItems)
      setBatchContent([...batchContent])
      setSelectedBatchContent(newItems)

      for (let newItem of newItems) {
        let generateOptions = {
          bcid: globalSettings.print?.raw?.bcid || 'code128',
          text: newItem._id,
          scale: 1,
          height: globalSettings.print?.raw?.height || 10,              // Bar height, in millimeters
          width: globalSettings.print?.raw?.width || 20,
          padding: globalSettings.print?.raw?.padding || 0,
          includetext: false,            // Show human-readable text
          textxalign: 'center',        // Always good to set this
        }

        console.log('generateOptions', generateOptions)
        /* await ipcRenderer.invoke('printPng_from_renderer', JSON.stringify(generateOptions))
        await delay(500) */
      }


    } catch (e) {
      notifications.showSystemError(e.message || e)
      console.error(e)
      return
    } finally {
      setLoading(false);
      setWeightsDivision([0, 0])
      setCurrentMode(null)
    }
  }

  // Применить агрегацию пакетов
  async function makeAggregation() {
    if (!selectedBatchContent.length) {
      notifications.showUserError('Пакеты не выбраны')
      return;
    }
    try {
      setLoading(true);
      let raws = selectedBatchContent.map(item => item._id);

      // console.log('aggregation, cat', selectedBatchContent[0].cat);

      let result = await helpers.submitObject("makeRawFromRaws", { raws, cat: selectedBatchContent[0].cat || {}, userId: userData.barcode || 'admin' })

      let newRawId = result.data.rawid
      let newItem = result.data.data

      let firstItemIndex = batchContent.indexOf(selectedBatchContent[0])
      batchContent = batchContent.filter(item => !selectedBatchContent.includes(item));
      batchContent.splice(firstItemIndex, 0, newItem)
      setBatchContent([...batchContent])
      setSelectedBatchContent([newItem])

      let generateOptions = {
        bcid: globalSettings.print?.raw?.bcid || 'code128',
        text: newRawId,
        scale: 1,
        height: globalSettings.print?.raw?.height || 10,              // Bar height, in millimeters
        width: globalSettings.print?.raw?.width || 20,
        padding: globalSettings.print?.raw?.padding || 0,
        includetext: false,            // Show human-readable text
        textxalign: 'center',        // Always good to set this
      }

      // bwipjs.toCanvas('mycanvas', generateOptions)

      console.log('generateOptions', generateOptions)
      // await ipcRenderer.invoke('printPng_from_renderer', JSON.stringify(generateOptions))
    } catch (e) {
      notifications.showSystemError(e.message || e)
      console.error(e)
      return
    } finally {
      setLoading(false);
      setWeightAggregation(0)
    }
  }

  //* endof обработчики

  return (
    // Обернули в label чтобы реализовать постоянный фокус нужного input, который обрабатывает qr-сканер. Сканер эмулируется через клавиатуру.
    <label htmlFor="scanInput" className="w-100" ref={elRef}>
      <TopBar backOption={true} title={title} />
      <div className="d-flex justify-content-between w-100 w-100 h-maxcontent bg-gray1">
        <input id="scanInput" autoFocus
          className="visually-hidden" type="text"
          onKeyDown={(e) => { if (e.key === 'Enter') helpers.handleEnterScan(e, handleScan) }} />

        {/* Левая панель с информацией о партии */}
        {(batchInfo.batch && !(isWeightChecked && isWeightCorrect) || '') &&
          <div className="d-flex flex-column justify-content-between align-items-center w-25 py-4 pl-4">
            <div className="left w-100 d-flex flex-column" >
              <div className="batch-info d-flex flex-column justify-content-between bg-white py-3 px-2 h-50 border-gray2 border-width-1 overflow-y-auto" style={{ height: '350px' }}>
                {batchInfo.batch &&
                  <div>
                    <div className="w-100 d-flex justify-content-between">
                      <span className="size-24 weight-700">ПАРТИЯ</span>
                      <span className="size-24 weight-700">#{batchInfo.batch?.barcode || '-'}</span>
                    </div>
                  </div>
                }

                <div className="d-flex flex-row justify-content-center">
                  <canvas className="w-75" id="scanCanvas"></canvas>
                </div>

                {batchInfo.batch &&
                  <div>
                    <div className="w-100 d-flex justify-content-between" style={{ marginTop: "25px", marginBottom: "5px" }}>
                      <span className="size-18 weight-700">Пакетов</span>
                      <span className="size-18 weight-700">Масса</span>
                    </div>
                    <div className="w-100 d-flex justify-content-between">
                      <span className="size-36 weight-700">{batchContent.length - selectedBatchContent.length || '-'} шт</span>
                      <span className="size-36 weight-700">{batchContent.length ? new Intl.NumberFormat('ru-RU').format(sumBatchWeight - sumSelectedBatchContentsWeight) : '-'} г</span>
                    </div>
                  </div>
                }
              </div>

              {(batchInfo.batch && isWeightChecked && !isWeightCorrect || '') &&
                <div className="w-100 border-purple border-width-3 bg-white">
                  <div
                    className={`w-100 d-flex all-center size-24 weight-700 text-center`}
                    style={{ height: '70px' }}
                  >
                    Масса после взвешивания
                  </div>

                  <div
                    className={`d-flex flex-row justify-content-between border-purple border-width-3 size-36 weight-700`}
                    style={{ height: '108px', margin: '-1px' }}
                  >
                    <div className="btn bg-purple color-white d-flex all-center border-width-0 size-48"
                      style={{ width: '104px', margin: '-1px' }}
                      onClick={e => setCorrectedWeight(correctedWeight - 1)}>
                      -
                    </div>
                    <div className="bg-white h-100 d-flex all-center flex-grow-1 size-36 weight-700">
                      <input type="number" value={correctedWeight} onChange={e => setCorrectedWeight(e.currentTarget.value)} className="bg-white h-100 border-width-0 text-center size-36 weight-700 no-number-arrows"
                        style={{ width: 40 + `${correctedWeight}`.length * 20 }}
                      /> &nbsp;г
                    </div>
                    <div className="btn bg-purple color-white d-flex all-center border-width-0 size-48"
                      style={{ width: '104px', margin: '-1px' }}
                      onClick={e => setCorrectedWeight(correctedWeight + 1)}>
                      +
                    </div>
                  </div>

                  <div
                    className={`w-100`}
                    style={{ height: '10px' }}
                  >
                  </div>
                </div>
              }

              <div className="d-flex flex-row" style={{ height: '120px' }}>
                {(batchInfo.batch && !isWeightCorrect || '') &&
                  <div
                    className={`h-100 flex-grow-1 btn bg-green color-white size-30 weight-700`}
                    onClick={e => handleCheckBatchWeight(true)}
                  >
                    Подтвердить
                  </div>
                }

                {(batchInfo.batch && isWeightChecked && !isWeightCorrect || '') &&
                  <div
                    className={`h-100 btn d-flex all-center bg-gray color-white size-30 weight-700 ml-1`}
                    style={{ width: '150px' }}
                    onClick={e => handleCancelWeightCorrecting()}
                  >
                    <img src={crossWhiteIcon} alt="" />
                  </div>
                }
              </div>


              {(batchInfo.batch && !isWeightChecked || '') &&
                <div
                  className={`w-100 btn bg-white color-red border-red border-width-7 size-30 weight-700 mt-1`}
                  style={{ height: '120px' }}
                  onClick={e => handleCheckBatchWeight(false)}
                >
                  Не совпадает
                </div>
              }


            </div>

            <div className="w-100 d-flex flex-column">

              {(batchInfo.batch && isWeightChecked && isWeightCorrect || '') &&
                <div className="info-block d-flex flex-row all-center w-100 bg-gray3 p-3 color-purple border-purple border-dashed" style={{ height: '150px' }}>
                  <img className="" src={infoIcon} alt="" style={{ width: '60px', height: 'auto' }} />
                  <span className="ml-4 size-30 weight-700" style={{ lineHeight: '1.5' }}>Выберите пакет с сырьем</span>
                </div>
              }
            </div>

          </div>
        }

        {/* Центральный блок, со списком пакетов или подсказками что делать */}
        <div className={`center d-flex all-center ${!batchContent.length ? 'w-100' : ((batchInfo.batch && isWeightChecked && isWeightCorrect) || (!batchInfo.batch && batchContent.length)) ? 'w-75' : 'w-50'} py-3`}>
          {(!batchContent.length || '') &&
            <div className="info-block d-flex flex-column all-center bg-white color-purple border-purple border-dashed" style={{ width: "440px", height: '350px' }}>
              <img className="" src={qrcodeIcon} alt="" style={{ width: '140px', height: 'auto' }} />
              <span className="mt-4 size-24 weight-500 text-center">Отсканируйте код партии или код пакета </span>
            </div>
          }

          {((batchInfo.batch && !isWeightChecked) || '') &&
            <div className="info-block d-flex flex-column all-center bg-white color-purple border-purple border-dashed" style={{ width: "440px", height: '350px' }}>
              <img className="" src={infoIcon} alt="" style={{ width: '140px', height: 'auto' }} />
              <span className="mt-5 size-24 weight-500">Определите массу сырья</span>
            </div>
          }

          {((batchInfo.batch && isWeightChecked && isWeightCorrect === false) || '') &&
            <div className="info-block d-flex flex-column all-center bg-white color-purple border-purple border-dashed" style={{ width: "440px", height: '350px' }}>
              <img className="" src={infoIcon} alt="" style={{ width: '140px', height: 'auto' }} />
              <span className="mt-5 size-24 weight-500">Введите массу после взвешивания</span>
            </div>
          }

          {((!batchInfo.batch && batchContent.length) || (batchInfo.batch && isWeightChecked && isWeightCorrect === true) || '') &&
            <div
              className="contents-block w-100 h-100 px-1 bg-gray1"
            >
              <div className="d-flex w-100 flex-row flex-wrap overflow-y-auto"
                style={{ justifyContent: "flex-start", alignItems: "flex-start", maxHeight: "calc(100% - 20px)" }}
              >
                {batchContentNotPrinted.map((item, idx) =>
                  <div key={item._id} className={`d-inline-block w-33 p-1`}>
                    {/* Много лишних рендеров при каждом действии */}
                    {/* {idx == 0 && batchContentItemRender++} */}
                    <div
                      className={`content-item w-100 d-flex flex-column btn bg-white p-2 border-gray2 border-width-1 ${selectedBatchContent.includes(item) && 'selected'}`}
                      style={{ height: '210px' }}
                      onClick={e => toggleSelectItem(item, idx)}
                    >
                      {item.process?.delta ?
                        <>
                          <div className="w-100 d-flex justify-content-between align-items-center">
                            <span className="d-flex flex-column align-items-start">
                              <span className="size-18 weight-700 text-left green-on-select">{categoryName(item.cat) || fishName(item.fish)}</span>
                              {/* <span className="size-14 weight-700 text-left">{item.fish}</span> */}
                            </span>
                            <span className="size-24 weight-700 text-right green-on-select">№ {item._id.replace(/^00/g, '').substr(0, 2)}</span>
                          </div>

                          <div className="w-100 d-flex justify-content-between mt-2">
                            <span className="size-18 weight-700 text-left">Текущий этап</span>
                            <span className="size-18 weight-700 text-right"></span>
                          </div>
                          <div className="w-100 d-flex justify-content-between mt-1">
                            <span className="size-18 weight-500 text-left">{item.process.name}</span>
                            <span className="size-18 weight-700 text-right"></span>
                          </div>

                          <div className="w-100 d-flex justify-content-between mt-2">
                            <span className="size-18 weight-700 text-left">Масса <span className="weight-500">{+item.weight?.toFixed(2)} г</span></span>
                            <span className="size-18 weight-700 text-right">Изменения &nbsp; <span className="weight-500">{+item.process.delta?.toFixed(2)}г ({+item.process.perc?.toFixed(2)}%)</span></span>
                          </div>
                        </>
                        :
                        <>
                          <div className="w-100 d-flex justify-content-between align-items-center">
                            <span className="d-flex flex-column align-items-start">
                              <span className="size-18 weight-700 text-left green-on-select">{categoryName(item.cat) || fishName(item.fish)}</span>
                              {/* <span className="size-14 weight-700 text-left">{item.fish}</span> */}
                            </span>
                            <span className="size-24 weight-700 text-right green-on-select">№ {item._id.replace(/^00/g, '').substr(0, 2)}</span>
                          </div>

                          <div className="w-100 d-flex justify-content-between mt-4">
                            <span className="size-18 weight-700 text-left">Время упаковки</span>
                            <span className="size-18 weight-700 text-right">Масса</span>
                          </div>

                          <div className="w-100 d-flex justify-content-between mt-1">
                            <span className="size-18 weight-500 text-left">{helpers.formatTime(item.date)}</span>
                            <span className="size-18 weight-500 text-right">{+item.weight?.toFixed(2) || '?'} г</span>
                          </div>
                        </>
                      }


                    </div>
                  </div>
                )}
              </div>
            </div>
          }
        </div>


        {(batchInfo.batch && !(isWeightChecked && isWeightCorrect) || '') && <div className="w-25"></div>}
        {/* Правая панель с меню операций */}
        {((batchInfo.batch && isWeightChecked && isWeightCorrect) || (!batchInfo.batch && batchContent.length) || '') &&
          // todo вынести в отдельный компонент, вместе с обработчиками, хотя бы внутренними. Подумать как.
          <div className="right d-flex flex-column w-25 h-100">
            {currentMode == null &&
              <div className="flex-grow-1 d-flex flex-column">
                <div className="products-info w-100 flex-grow-1 pb-3 px-5 bg-white border-gray2 border-width-1 overflow-y-auto" style={{ height: "425px", borderTop: 0 }}>
                  <div className="d-flex align-items-center text-uppercase size-24 weight-700" style={{ height: '95px' }}>Выбор процесса</div>
                  {allModes.map(modeItem =>
                    <div key={modeItem.name} disabled={modeItem.isDisabled} className={`btn right-menu-item ${modeItem == currentMode && 'selected'} w-100 d-flex flex-row justify-content-between align-items-center bg-gray3 border-gray2 border-width-1 py-3 px-4`}
                      style={{ height: '87px', marginTop: '5px', }}
                      onClick={e => handleSetMode(modeItem)}
                    >
                      <span className="size-24 weight-700">{modeItem.title}</span>
                      <span className="size-48 weight-500 color-green">&#8250;</span>
                    </div>
                  )
                  }
                </div>


                <div className="product-info w-100 py-4 px-5 bg-gray3 border-gray2 border-width-1" style={{ height: "300px", borderTopWidth: 0 }}>
                  {(batchInfo.batch || batchContent.length || '') &&
                    <div className="h-100 d-flex flex-column justify-content-between">
                      <div className="w-100">

                        <div className="w-100 d-flex justify-content-between mt-3 align-items-baseline" >
                          <span className="size-24 weight-700">Выберите процесс</span>
                        </div>
                      </div>

                      <div className="w-100">
                        <div className="w-100 d-flex justify-content-between align-items-baseline mt-1" >
                          {/* № {(selectedBatchContents.length < 9) ? selectedBatchContents.map(item => item._id.substr(0, 2)).join(', ') || '-' : '... ' + selectedBatchContents.slice(selectedBatchContents.length - 8).map(item => item._id.substr(0, 2)).join(', ')} */}
                          <span className="w-50 text-left size-18 weight-700">Масса продукта</span>
                          <span className="w-50 text-right size-36 weight-700">{selectedBatchContent.filter(item => !item.printed).length ? +selectedBatchContent.filter(item => !item.printed).reduce((sum, item) => sum + item.weight, 0).toFixed(2) : '-'} г</span>
                        </div>

                      </div>

                    </div>
                  }

                </div>
              </div>
            }{/* endof mode null */}

            {currentMode?.name == 'aggregation' &&
              <div className="flex-grow-1 d-flex flex-column">
                <div className="products-info w-100 flex-grow-1 pb-3 px-5 bg-white border-gray2 border-width-1 overflow-y-auto" style={{ height: "425px", borderTop: 0 }}>
                  <div className="d-flex align-items-center text-uppercase size-24 weight-700" style={{ height: '95px' }}>{currentMode.title}</div>
                  {allModes.map(modeItem =>
                    <div key={modeItem.name} className={`btn right-menu-item ${modeItem.name == currentMode.name && 'selected'} w-100 d-flex flex-row justify-content-between align-items-center bg-gray3 border-gray2 border-width-1 py-3 px-4`}
                      style={{ height: '87px', marginTop: '5px', }}
                      onClick={e => handleSetMode(modeItem)}
                    >
                      <span className="size-24 weight-700">{modeItem.title}</span>
                      <span className="size-48 weight-500 color-green">&#8250;</span>
                    </div>
                  )
                  }
                </div>


                <div className="product-info w-100 py-4 px-5 bg-gray3 border-gray2 border-width-1" style={{ height: "300px", borderTopWidth: 0 }}>
                  <div className="h-100 d-flex flex-column justify-content-between">
                    <div className="w-100">

                      <div className="w-100 d-flex justify-content-between mt-3 align-items-baseline" >
                        <span className="size-24 weight-700">{currentMode?.title} ({selectedBatchContentNotPrinted.length})</span>
                      </div>
                    </div>

                    <div className="w-100">
                      <div className="w-100 d-flex justify-content-between align-items-baseline mt-1" >
                        <span className="w-50 text-left size-18 weight-700">Масса продукта</span>
                        <span className="w-50 text-right size-36 weight-700">{weightAggregation ? +weightAggregation.toFixed(2) + ' г' : 'Ожидание'}</span>
                      </div>

                      <div
                        className={`w-100 btn bg-green color-white size-30 weight-700 mt-3 ${isLoading && 'loading'}`}
                        disabled={!weightAggregation}
                        style={{ height: '90px' }}
                        onClick={e => makeAggregation()}
                      >
                        {isLoading && <span>&nbsp; </span>}
                        <div className="loader lds-ring size-30 color-white">
                          <div></div>
                          <div></div>
                          <div></div>
                        </div> Применить
                      </div>
                    </div>

                  </div>

                </div>
              </div>
            }{/* endof mode aggregation */}

            {currentMode?.name == 'division' &&
              <div className="flex-grow-1 d-flex flex-column">
                <div className="products-info w-100 flex-grow-1 pb-3 px-5 bg-white border-gray2 border-width-1 overflow-y-auto" style={{ height: "425px", borderTop: 0 }}>
                  <div className="d-flex align-items-center text-uppercase size-24 weight-700" style={{ height: '95px' }}>{currentMode.title}</div>
                  {weightsDivision.map((item, idx) =>
                    <div key={idx} className={`right-menu-item w-100 d-flex flex-row justify-content-between align-items-center bg-gray3 border-gray2 border-width-1 py-3 px-4`}
                      style={{ height: '87px', marginTop: '5px', }}
                    >
                      <span className="size-24 weight-700">Продукт №{idx + 1}</span>
                      <span className="size-24 weight-700">{item || 'Ожидание'}</span>
                    </div>
                  )
                  }
                  <div className={`btn w-100 d-flex flex-row justify-content-between align-items-center bg-yellow border-gray2 border-width-1 py-3 px-4`}
                    style={{ height: '87px', marginTop: '5px', }}
                    onClick={e => addWeightsDivision()}
                  >
                    <span className="size-24 weight-700">Добавить продукт №{weightsDivision.length + 1}</span>
                  </div>
                </div>


                <div className="product-info w-100 py-4 px-5 bg-white border-gray2 border-width-1" style={{ borderTopWidth: 0 }}>
                  <div className="h-100 d-flex flex-column justify-content-between">


                    <div className="w-100">
                      <div
                        className={`w-100 btn bg-green color-white size-30 weight-700 mt-3 ${isLoading && 'loading'}`}
                        disabled={weightsDivision.filter(weight => weight).length != weightsDivision.length}
                        style={{ height: '90px' }}
                        onClick={e => makeDivision()}
                      >
                        {isLoading && <span>&nbsp; </span>}
                        <div className="loader lds-ring size-30 color-white">
                          <div></div>
                          <div></div>
                          <div></div>
                        </div> Печатать коды
                      </div>

                      <div
                        className={`w-100 btn bg-gray color-white size-30 weight-700 mt-3`}
                        style={{ height: '90px' }}
                        onClick={e => handleSetMode(null)}
                      >
                        Отмена
                      </div>
                    </div>

                  </div>

                </div>
              </div>
            }{/* endof mode division */}

            {currentMode?.name == 'packer' &&
              <div className="flex-grow-1 d-flex flex-column">
                <div className="products-info w-100 flex-grow-1 pb-3 px-5 bg-white border-gray2 border-width-1 overflow-y-auto" style={{ height: "425px", borderTop: 0 }}>
                  <div className="d-flex align-items-center text-uppercase size-24 weight-700" style={{ height: '95px' }}>{currentMode.title}</div>
                  {allModes.map(modeItem =>
                    <div key={modeItem.name} className={`btn right-menu-item ${modeItem.name == currentMode.name && 'selected'} w-100 d-flex flex-row justify-content-between align-items-center bg-gray3 border-gray2 border-width-1 py-3 px-4`}
                      style={{ height: '87px', marginTop: '5px', }}
                      onClick={e => handleSetMode(modeItem)}
                    >
                      <span className="size-24 weight-700">{modeItem.title}</span>
                      <span className="size-48 weight-500 color-green">&#8250;</span>
                    </div>
                  )
                  }
                </div>

                <div className="product-info w-100 py-4 px-5 bg-gray3 border-gray2 border-width-1" style={{ height: "300px", borderTopWidth: 0 }}>
                  <div className="h-100 d-flex flex-column justify-content-between">
                    <div className="w-100">

                      <div className="w-100 d-flex justify-content-between mt-3 align-items-baseline" >
                        <span className="size-24 weight-700">{currentMode?.title} ({selectedBatchContentNotPrinted.length})</span>
                      </div>
                    </div>

                    <div className="w-100">
                      <div className="w-100 d-flex justify-content-between align-items-baseline mt-1" >
                        <span className="w-50 text-left size-18 weight-700">Масса продукта</span>
                        <span className="w-50 text-right size-36 weight-700">{weightPacker ? +weightPacker.toFixed(2) + ' г' : 'Ожидание'}</span>
                      </div>

                      <div
                        className={`w-100 btn bg-green color-white size-30 weight-700 mt-3 ${isLoading && 'loading'}`}
                        disabled={!weightPacker}
                        style={{ height: '90px' }}
                        onClick={e => sendToPacker()}
                      >
                        {isLoading && <span>&nbsp; </span>}
                        <div className="loader lds-ring size-30 color-white">
                          <div></div>
                          <div></div>
                          <div></div>
                        </div> Подтвердить
                      </div>
                    </div>

                  </div>

                </div>
              </div>
            }{/* endof mode packer */}

            {currentMode?.name == 'operations' &&
              <div className="flex-grow-1 d-flex flex-column">
                <div className="products-info w-100 flex-grow-1 pb-3 px-5 bg-white border-gray2 border-width-1 overflow-y-auto" style={{ height: "425px", borderTop: 0 }}>
                  <div className="d-flex align-items-center text-uppercase size-24 weight-700" style={{ height: '95px' }}>{currentMode.title}</div>
                  {allOperations.map(operation =>
                    <div key={operation.id} className={`btn right-menu-item ${operation.type == selectedOperation?.type && 'selected'} w-100 d-flex flex-row justify-content-between align-items-center bg-gray3 border-gray2 border-width-1 py-3 px-4`}
                      style={{ height: '87px', marginTop: '5px', }}
                      onClick={e => setSelectedOperation(operation)}
                    >
                      <span className="size-24 weight-700">{operation.name}</span>
                      <span className="size-48 weight-500 color-green">&#8250;</span>
                    </div>
                  )
                  }
                </div>

                <div className="product-info w-100 py-4 px-5 bg-gray3 border-gray2 border-width-1" style={{ borderTopWidth: 0 }}>
                  <div className="h-100 d-flex flex-column justify-content-between">
                    <div className="w-100">

                      <div className="w-100 d-flex justify-content-between mt-3 align-items-baseline" >
                        <span className="size-24 weight-700">{selectedOperation?.name || 'Выберите операцию'} ({selectedBatchContentNotPrinted.length})</span>
                      </div>
                    </div>

                    <div className="w-100">
                      <div className="w-100 d-flex justify-content-between align-items-baseline mt-1" >
                        <span className="w-50 text-left size-18 weight-700">Масса продукта</span>
                        <span className="w-50 text-right size-36 weight-700">{weightOperations ? +weightOperations.toFixed(2) + ' г' : 'Ожидание'}</span>
                      </div>

                      <div
                        className={`w-100 btn bg-green color-white size-30 weight-700 mt-3 ${isLoading && 'loading'}`}
                        disabled={!weightOperations}
                        style={{ height: '90px' }}
                        onClick={e => makeOperation()}
                      >
                        {isLoading && <span>&nbsp; </span>}
                        <div className="loader lds-ring size-30 color-white">
                          <div></div>
                          <div></div>
                          <div></div>
                        </div> Применить
                      </div>
                      <div
                        className={`w-100 btn bg-gray color-white size-30 weight-700 mt-3`}
                        style={{ height: '90px' }}
                        onClick={e => handleSetMode(null)}
                      >
                        Отмена
                      </div>
                    </div>

                  </div>

                </div>
              </div>
            }{/* endof mode packer */}


            {currentMode?.name == 'recipe' &&
              <div className="flex-grow-1 d-flex flex-column justify-content-between">
                <div className="products-info w-100 flex-grow-1 pb-3 px-5 bg-white border-gray2 border-width-1 overflow-y-auto" style={{ height: "425px", borderTop: 0 }}>
                  <div className="d-flex align-items-center text-uppercase size-24 weight-700" style={{ height: '95px' }}>{currentMode.title}</div>

                  {selectedCatTab == 'group' &&
                    preparedCategories.map(item =>
                      <div key={item._id || item.name} className="btn w-100 d-flex flex-row justify-content-between align-items-center bg-gray3 border-gray2 border-width-1 py-3 px-4"
                        style={{ height: '87px', marginTop: '5px', }}
                        onClick={e => selectCatNode(e, item, 'group')}
                      >
                        <span className="size-24 weight-700">{item.name}</span>
                        <span className="size-48 weight-500 color-green">&#8250;</span>
                      </div>
                    )
                  }

                  {selectedCatTab == 'grade' &&
                    allGrades.map(item =>
                      <div key={item._id || item.name} className="btn w-100 d-flex flex-row justify-content-between align-items-center bg-gray3 border-gray2 border-width-1 py-3 px-4"
                        style={{ height: '87px', marginTop: '5px', }}
                        onClick={e => selectCatNode(e, item, 'grade')}
                      >
                        <span className="size-24 weight-700">{item.name}</span>
                        <span className="size-48 weight-500 color-green">&#8250;</span>
                      </div>
                    )
                  }

                </div>


                <div className="d-flex flex-column w-100 p-5 bg-white border-gray2 border-width-1" style={{ borderTop: 0 }}>
                  {!isFirstCatTab &&
                    <div className="w-100 size-30 weight-700 mb-2 btn d-flex all-center bg-yellow color-black" style={{ height: '90px' }}
                      onClick={e => selectParentCatNode(e)}>
                      <img src={arrowLeftBlack} className="mr-4" alt="" style={{ width: '40px', marginLeft: '-50px' }} /> Назад
                    </div>
                  }
                  <div
                    className={`w-100 btn bg-gray color-white size-30 weight-700`}
                    style={{ height: '90px' }}
                    onClick={e => handleSetMode(null)}
                  >
                    Отмена
                  </div>
                </div>
              </div>
            }{/* endof mode recipe */}

          </div>
        }
      </div>


    </label>
  );
}
