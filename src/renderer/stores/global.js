import _ from "lodash";
import { createStore, createApi, combine, createEvent } from "effector";
// тут еще есть домены, но пока что состояние только глобальное

// это скорее dictionaries. Обычный global где есть reset надо в другой файл
// setGlobalInfo нужно минимум для версии
const globalReset = createEvent();
export { globalReset };

let loginDataDefault = {};
const $loginData = createStore(loginDataDefault);
$loginData.api = createApi($loginData, {
  replace(state, payload) {
    if (payload == undefined) payload = _.clone(state);
    return payload;
  },
  update(state, payload) {
    return { ...state, ...payload };
  },
  reset(state, payload) {
    // потому что объект при init не копируется
    // effector группирует обновления также внутри своих событий, так и в Реакте должно быть
    return _.cloneDeep(loginDataDefault);
  },
});
export { $loginData };
window.$loginData = $loginData;

let userDataDefault = {};
const $userData = createStore(userDataDefault);
$userData.api = createApi($userData, {
  replace(state, payload) {
    if (payload == undefined) payload = _.clone(state);
    return payload;
  },
  update(state, payload) {
    return { ...state, ...payload };
  },
  reset(state, payload) {
    // потому что объект при init не копируется
    // effector группирует обновления также внутри своих событий, так и в Реакте должно быть
    return _.cloneDeep(userDataDefault);
  },
});
export { $userData };
window.$userData = $userData;

let nfcStatusDefault = null;
const $nfcStatus = createStore(_.cloneDeep(nfcStatusDefault)).on(
  globalReset,
  () => {
    return _.cloneDeep(nfcStatusDefault);
  }
);
$nfcStatus.api = createApi($nfcStatus, {
  replace(state, payload) {
    if (payload == undefined) payload = _.clone(state);
    return payload;
  },
  update(state, payload) {
    return { ...state, ...payload };
  },
  reset(state, payload) {
    // потому что объект при init не копируется
    // effector группирует обновления также внутри своих событий, так и в Реакте должно быть
    return _.cloneDeep(nfcStatusDefault);
  },
});
export { $nfcStatus };
window.$nfcStatus = $nfcStatus;

const setRawCategories = createEvent();
const $rawCategories = createStore([]).on(
  setRawCategories,
  (state, payload) => {
    window.$rawCategories = payload;
    return payload;
  }
);
export { $rawCategories, setRawCategories };

const setTara = createEvent();
const $tara = createStore([]).on(setTara, (state, payload) => {
  window.$tara = payload;
  return payload;
});
export { $tara, setTara };

const setProcess = createEvent();
const $process = createStore([]).on(setProcess, (state, payload) => {
  window.$process = payload;
  return payload;
});
export { $process, setProcess };

const setFishTypes = createEvent();
const $fishTypes = createStore([]).on(setFishTypes, (state, payload) => {
  window.$fishTypes = payload;
  return payload;
});
export { $fishTypes, setFishTypes };

const $requestsPending = createStore(0);
$requestsPending.api = createApi($requestsPending, {
  increase(state, number) {
    return state + number;
  },
  decrease(state, number) {
    return state - number;
  },
});

export { $requestsPending };

function getGlobalSettings() {
  let stored = localStorage.getItem("$globalSettings") || "{}";

  let parsed = JSON.parse(stored);
  if (!Object.keys(parsed).length) {
    parsed = {
      print: {
        raw: {
          height: 10,
          padding: 0,
          width: "auto",
          bcid: "code128",
        },
        item: {
          height: 20,
          padding: 0,
          width: 20,
          bcid: "qrcode",
        },
        batch: {
          height: 10,
          padding: 0,
          width: "auto",
          bcid: "code128",
        },

        page: {
          height: "fit-content",
          width: "fit-content",
        },
      },
    };
  }
  return parsed;
}

const setGlobalSettings = createEvent();
const $globalSettings = createStore(getGlobalSettings()).on(
  setGlobalSettings,
  (state, payload) => {
    window.$globalSettings = payload;
    localStorage.setItem("$globalSettings", JSON.stringify(payload));
    return payload;
  }
);
export { $globalSettings, setGlobalSettings };
window.setGlobalSettings = setGlobalSettings; // для удобного теста

const setGlobalInfo = createEvent();
const $globalInfo = createStore({}).on(setGlobalInfo, (state, payload) => {
  window.$globalInfo = payload;
  console.log("set $globalInfo", payload);
  return payload;
});
export { $globalInfo, setGlobalInfo };
window.setGlobalInfo = setGlobalInfo; // для удобного теста

function getRouteData() {
  let stored = localStorage.getItem("$routeData") || "{}";

  let parsed = JSON.parse(stored);

  return parsed;
}

const routeDataDefault = {}
const $routeData = createStore(getRouteData());
$routeData.api = createApi($routeData, {
  replace(state, payload) {
    if (payload == undefined) payload = _.clone(state);
    localStorage.setItem('$routeData', JSON.stringify(payload))
    return payload;
  },
  update(state, payload) {
    let newState = { ...state, ...payload }
    localStorage.setItem('$routeData', JSON.stringify(newState))
    return newState;
  },
  reset(state, payload) {
    // потому что объект при init не копируется
    // effector группирует обновления также внутри своих событий, так и в Реакте должно быть
    localStorage.setItem('$routeData', JSON.stringify(routeDataDefault))
    return _.cloneDeep(routeDataDefault);
  },
});
export { $routeData };
window.$routeData = $routeData; // для удобного теста
